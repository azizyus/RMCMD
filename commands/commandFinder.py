
from commands.showTotalConnectedClientList import showTotalConnectedClientList
from commands.clearScreenCls import clearScreenCls
from commands.clearScreenClear import clearScreenClear
from commands.chooser import chooser

from myClient import myClient
from pprint import pprint
from factories.clientFactory import clientFactory

import shlex


class commandFinder:


    @staticmethod
    def commands():
        # COMMANDS LIST
        commands = {

            "list-users": showTotalConnectedClientList(),
            "lu": showTotalConnectedClientList(),
            "ls": showTotalConnectedClientList(),
            "cls":clearScreenCls(),
            "clear":clearScreenClear(),
            "chooser":chooser()


        }
        # END COMMANDS LIST
        return commands


    @staticmethod
    def findCommand(command,generalData):

        fullCommand = shlex.split(command)
        firstCommand = fullCommand[0]
        if firstCommand in commandFinder.commands():
            commandInstance = commandFinder.commands()[firstCommand]

            methodTocall = getattr(commandInstance, "executeCommand")
            result = methodTocall(fullCommand,generalData)
            print ("FULL COMMAND -> "+str(fullCommand))
            pprint ("MAIN COMMAND -> "+str(firstCommand))
        else:
            print ("COMMAND NOT FOUND")


